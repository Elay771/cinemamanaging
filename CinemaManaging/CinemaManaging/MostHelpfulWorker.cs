﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MostHelpfulWorker : Form
    {
        DB db;
        DateTime  from, to;
        public MostHelpfulWorker(DB db, DateTime from, DateTime to)
        {
            this.from = from;
            this.to = to;
            this.db = db;
            InitializeComponent();
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void MostHelpfulWorker_Load(object sender, EventArgs e)
        {
            int max = -1;
            int maxIndex = -1;
            db.Query("Select * from MovieOrders");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {

                db.Query("Select * from TheaterMovies where TheaterMovieID=" + DB.ds.Tables[0].Rows[i]["TheaterMovieID"] + ";");
                string date = DB.ds.Tables[0].Rows[0]["StartTime"].ToString().Substring(0, 10);
                DateTime currDate = DateTime.Parse(date);
                if (DateTime.Compare(from, currDate) <= 0 && DateTime.Compare(to, currDate) >= 0)
                {
                    db.WorkersData();
                    int curr;
                    db.Query("SELECT COUNT(*) from MovieOrders where WorkerID=" + DB.ds.Tables[0].Rows[i]["WorkerID"].ToString() + ";");
                    curr = Int32.Parse(DB.ds.Tables[0].Rows[0][0].ToString());
                    if (max < curr)
                    {
                        max = curr;
                        maxIndex = i;
                    }
                    db.Query("Select * from MovieOrders");
                }

            }
            db.WorkersData();

            if(maxIndex>=0)
            {
                label18.Hide();
                label7.Text = DB.ds.Tables[0].Rows[maxIndex]["WorkerID"].ToString();
                label8.Text = DB.ds.Tables[0].Rows[maxIndex]["WorkerName"].ToString();
                label9.Text = DB.ds.Tables[0].Rows[maxIndex]["Age"].ToString();
                label10.Text = DB.ds.Tables[0].Rows[maxIndex]["JoinningDate"].ToString();
                label11.Text = DB.ds.Tables[0].Rows[maxIndex]["Position"].ToString();
                label12.Text = DB.ds.Tables[0].Rows[maxIndex]["SalaryPerHour"].ToString();
                label15.Text = DB.ds.Tables[0].Rows[maxIndex]["Password"].ToString();
                label17.Text = DB.ds.Tables[0].Rows[maxIndex]["Email"].ToString();
            }
            else
            {
                label2.Hide();
                label3.Hide();
                label4.Hide();
                label5.Hide();
                label6.Hide();
                label13.Hide();
                label14.Hide();
                label16.Hide();
                label7.Hide();
                label8.Hide();
                label9.Hide();
                label10.Hide();
                label11.Hide();
                label12.Hide();
                label15.Hide();
                label17.Hide();
                label18.Show();
            }
            
        }
    }
}
