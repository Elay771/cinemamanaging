﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;//we must to add
using System.Data;
using System.IO;
namespace CinemaManaging
{
    
        public class DB
        {
            public DB()
            {
                string path = AppDomain.CurrentDomain.BaseDirectory;
                path = path.Substring(0, path.Length - 10);
                path += "DataBase.mdb";

                Connect(path);
            }

            public static System.Data.DataSet ds;
            public static OleDbConnection objConn;
            public bool Connect(string DBFile)
            
        {
                try
                {
                    objConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + DBFile);
                    return (objConn.State == System.Data.ConnectionState.Open);
                }
                catch (Exception ex)
                {
                    //System.Windows.Forms.MessageBox.Show(ex.Message);
                    return false;
                }
            }

            public bool Query(string sqlStr)
            {
                try
                {
                    ds = new System.Data.DataSet();
                    OleDbDataAdapter da = new OleDbDataAdapter(sqlStr, objConn);
                    da.Fill(ds);
                    return true;
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    return false;
                }
            }

            public bool isPasswordCorrect(string WorkerID, string password)
            {
                Query("Select Count(*) from Workers where Password='" + password + "' AND WorkerID=" + WorkerID + ";");
                return ds.Tables[0].Rows[0][0].ToString() != "0";
            }

            public string getUserPosition(string WorkerID)
            {
                Query("Select * from Workers where WorkerID=" + WorkerID + ";");
                return ds.Tables[0].Rows[0][4].ToString();
            }

            public bool doesUserExist(string WorkerID)
            {
                Query("Select Count(*) from Workers where WorkerID=" + WorkerID + ";");
                return ds.Tables[0].Rows[0][0].ToString() != "0";
            }

            public void addUserToDB(string name,string WorkerID, string password, string email,string age, string Position)
            {
                Query("INSERT INTO Workers ([WorkerID],[WorkerName],[Password],[Email],[Age],[Position],[SalaryPerHour],[JoinningDate]) VALUES(" + WorkerID + ", '" + name + "', '" + password + "', '" + email + "',"+age+",'"+Position+"',"+ Salary.getSalary( Position,  Convert.ToInt32(age))+",'"+Times.getTodaysData()+"');");
            }
            public void WorkersData()
            {
                Query("Select * from Workers;");
            }
            public void CustomersData()
            {
                Query("Select * from Customers;");
            }
            public void MovieOrdersData()
            {
                Query("Select * from MovieOrders;");
            }
    }
}

