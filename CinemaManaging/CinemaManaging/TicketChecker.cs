﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class TicketChecker : Form
    {
        DB db;

        string WorkerID;
        public TicketChecker(DB db, string WorkerID)
        {
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void TicketChecker_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text!="")
            {
                this.Hide();
                var next = new MovieOrdersOfClientShow(db, Int32.Parse(textBox1.Text));
                next.Closed += (s, args) => this.Close();
                next.Show();
            }
        }
    }
}
