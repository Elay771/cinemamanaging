﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class CustomerDataShow : Form
    {
        DB db;
        public CustomerDataShow(DB db)
        {
            this.db = db;
            InitializeComponent();
        }

        private void CustomerDataShow_Load(object sender, EventArgs e)
        {
            RefreshCustomersTable();
        }

        private void RefreshCustomersTable()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();

            db.CustomersData();
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                listBox1.Items.Add(DB.ds.Tables[0].Rows[i]["ClientID"].ToString());
                listBox2.Items.Add(DB.ds.Tables[0].Rows[i]["ClientName"].ToString());
                listBox3.Items.Add(DB.ds.Tables[0].Rows[i]["Age"].ToString());
                listBox4.Items.Add(DB.ds.Tables[0].Rows[i]["ClientEmail"].ToString());
                listBox5.Items.Add(DB.ds.Tables[0].Rows[i]["ClientAddress"].ToString());
            }
        }
        private void Refresh_Click(object sender, EventArgs e)
        {
            RefreshCustomersTable();
        }
        

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            textBox1.Text = listBox1.Items[listBox1.SelectedIndex].ToString();
            textBox2.Text = listBox2.Items[listBox1.SelectedIndex].ToString();
            textBox3.Text = listBox3.Items[listBox1.SelectedIndex].ToString();
            textBox4.Text = listBox4.Items[listBox1.SelectedIndex].ToString();
            textBox5.Text = listBox5.Items[listBox1.SelectedIndex].ToString();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                listBox1.SelectedItem = listBox1.Items[0];
            string strSql = "DELETE FROM Customers WHERE ClientID=" + listBox1.SelectedItem.ToString() + "";
            db.Query(strSql);
            MessageBox.Show("Customer with ID " + listBox1.SelectedItem.ToString() + " deleted");
            RefreshCustomersTable();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                listBox1.SelectedItem = listBox1.Items[0];
            if (textBox1.Text != "" && textBox2.Text != "" &&
                textBox3.Text != "" &&
                textBox4.Text != "" &&
                textBox5.Text != "")
            {
                bool isSup = false;
                db.Query("Select * from Customers");
                for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
                {

                    if (listBox1.SelectedItem.ToString() == DB.ds.Tables[0].Rows[i]["ClientID"].ToString())
                    {
                        isSup = true;
                        break;
                    }

                }
                if (isSup)
                {
                    List<string> statements = new List<string>();
                    statements.Add("UPDATE Customers new SET [ClientName]='" + textBox2.Text + "'" + " WHERE ClientID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    statements.Add("UPDATE Customers new SET [Age]=" + textBox3.Text + " WHERE ClientID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    statements.Add("UPDATE Customers new SET [ClientEmail]='" + textBox4.Text + "'" + " WHERE ClientID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    statements.Add("UPDATE Customers new SET [ClientAddress]='" + textBox5.Text + "'" + " WHERE ClientID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    statements.Add("UPDATE Customers new SET [ClientID]=" + textBox1.Text + " WHERE ClientID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    for (int i = 0; i < 5; i++)
                        db.Query(statements.ToArray()[i]);
                    MessageBox.Show("Client updated succesfully");
                    RefreshCustomersTable();
                }
                else
                    MessageBox.Show("Client with ID: " + textBox1.Text + " is not in the system");
            }
        }
    }
}
