﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class TicketSeller : Form
    {
        DB db;
        string WorkerID;
        public TicketSeller(DB db, string WorkerID)
        {
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void TicketSeller_Load(object sender, EventArgs e)
        {

        }

        private void OrderTicket_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new MovieTicketOrder(db,WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }
    }
}
