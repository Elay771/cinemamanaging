﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MovieOrdersOfClientShow : Form
    {
        DB db;
        int clientID;
        public MovieOrdersOfClientShow(DB db, int clientID)
        {
            this.clientID = clientID;
            this.db = db;
            InitializeComponent();
        }

        private void MovieOrdersShow_Load(object sender, EventArgs e)
        {
            RefreshMovieOrdersTable();
        }
        private void RefreshMovieOrdersTable()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();
            listBox6.Items.Clear();
            listBox7.Items.Clear();

            db.Query("Select * from MovieOrders where ClientID="+ clientID + ";");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                listBox1.Items.Add(DB.ds.Tables[0].Rows[i]["MovieOrderID"].ToString());
                listBox2.Items.Add(DB.ds.Tables[0].Rows[i]["TheaterMovieID"].ToString());
                listBox3.Items.Add(DB.ds.Tables[0].Rows[i]["Row"].ToString());
                listBox4.Items.Add(DB.ds.Tables[0].Rows[i]["Col"].ToString());
                listBox5.Items.Add(DB.ds.Tables[0].Rows[i]["WorkerID"].ToString());
                listBox6.Items.Add(DB.ds.Tables[0].Rows[i]["ClientID"].ToString());
                db.Query("Select * from TheaterMovies where TheaterMovieID=" + DB.ds.Tables[0].Rows[i]["TheaterMovieID"].ToString() + ";");
                listBox7.Items.Add(DB.ds.Tables[0].Rows[i]["StartTime"].ToString());
            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            RefreshMovieOrdersTable();
        }

       
    }
}
