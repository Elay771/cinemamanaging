﻿namespace CinemaManaging
{
    partial class TicketSeller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OrderTicket = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // OrderTicket
            // 
            this.OrderTicket.Location = new System.Drawing.Point(328, 214);
            this.OrderTicket.Name = "OrderTicket";
            this.OrderTicket.Size = new System.Drawing.Size(144, 23);
            this.OrderTicket.TabIndex = 7;
            this.OrderTicket.Text = "Order a Movie Ticket";
            this.OrderTicket.UseVisualStyleBackColor = true;
            this.OrderTicket.Click += new System.EventHandler(this.OrderTicket_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(75, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 31);
            this.label1.TabIndex = 8;
            this.label1.Text = "Hello Tickets Seller";
            // 
            // TicketSeller
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OrderTicket);
            this.Name = "TicketSeller";
            this.Text = "TicketSeller";
            this.Load += new System.EventHandler(this.TicketSeller_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OrderTicket;
        private System.Windows.Forms.Label label1;
    }
}