﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class WorkersDataShow : Form
    {
        DB db;
        public WorkersDataShow(DB db)
        {
            this.db = db;
            InitializeComponent();
        }

        private void WorkersDataShow_Load(object sender, EventArgs e)
        {
            RefreshWorkersTable();
        }
        private void RefreshWorkersTable()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();
            listBox6.Items.Clear();
            listBox7.Items.Clear();
            listBox8.Items.Clear();

            db.WorkersData();
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                listBox1.Items.Add(DB.ds.Tables[0].Rows[i]["WorkerID"].ToString());
                listBox2.Items.Add(DB.ds.Tables[0].Rows[i]["WorkerName"].ToString());
                listBox3.Items.Add(DB.ds.Tables[0].Rows[i]["Age"].ToString());
                listBox4.Items.Add(DB.ds.Tables[0].Rows[i]["JoinningDate"].ToString());
                listBox5.Items.Add(DB.ds.Tables[0].Rows[i]["Position"].ToString());
                listBox6.Items.Add(DB.ds.Tables[0].Rows[i]["SalaryPerHour"].ToString());
                listBox7.Items.Add(DB.ds.Tables[0].Rows[i]["Password"].ToString());
                listBox8.Items.Add(DB.ds.Tables[0].Rows[i]["Email"].ToString());
            }
        }
        private void Refresh_Click(object sender, EventArgs e)
        {
            RefreshWorkersTable();
        }

        

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                listBox1.SelectedItem=listBox1.Items[0];
            if (textBox1.Text!=""&& textBox2.Text != "" &&
                textBox6.Text != "" && textBox3.Text != "" &&
                textBox7.Text != "" && textBox4.Text != "" &&
                textBox8.Text != "" && textBox5.Text != "")
            {
            bool isSup = false;
            db.Query("Select * from Workers");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                if (listBox1.SelectedItem.ToString() == DB.ds.Tables[0].Rows[i]["WorkerID"].ToString())
                {
                    isSup = true;
                    break;
                }

            }
            if (isSup)
            {
                List<string> statements=new List<string>();
                statements.Add("UPDATE Workers new SET [WorkerName]='" + textBox2.Text + "'" + " WHERE WorkerID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                statements.Add("UPDATE Workers new SET [Age]=" + textBox3.Text + " WHERE WorkerID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                statements.Add("UPDATE Workers new SET [JoinningDate]='" + textBox4.Text + "'" + " WHERE WorkerID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                statements.Add("UPDATE Workers new SET [Position]='" + textBox5.Text + "'" + " WHERE WorkerID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                statements.Add("UPDATE Workers new SET [SalaryPerHour]=" + textBox6.Text + " WHERE WorkerID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                statements.Add("UPDATE Workers new SET [Password]='" + textBox7.Text + "'" + " WHERE WorkerID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                statements.Add("UPDATE Workers new SET [Email]='" + textBox8.Text + "'" + " WHERE WorkerID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                statements.Add("UPDATE Workers new SET [WorkerID]=" + textBox1.Text + " WHERE WorkerID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                for (int i = 0; i < 8; i++)
                    db.Query(statements.ToArray()[i]);
                MessageBox.Show("Worker updated succesfully");
                RefreshWorkersTable();
            }
            else
                MessageBox.Show("Worker with ID: " + textBox1.Text + " is not in the system");
        }
    }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Text = listBox1.Items[listBox1.SelectedIndex].ToString();
            textBox2.Text = listBox2.Items[listBox1.SelectedIndex].ToString();
            textBox3.Text = listBox3.Items[listBox1.SelectedIndex].ToString();
            textBox4.Text = listBox4.Items[listBox1.SelectedIndex].ToString();
            textBox5.Text = listBox5.Items[listBox1.SelectedIndex].ToString();
            textBox6.Text = listBox6.Items[listBox1.SelectedIndex].ToString();
            textBox7.Text = listBox7.Items[listBox1.SelectedIndex].ToString();
            textBox8.Text = listBox8.Items[listBox1.SelectedIndex].ToString();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                listBox1.SelectedItem = listBox1.Items[0];
            string strSql = "DELETE FROM Workers WHERE WorkerID =" + listBox1.SelectedItem.ToString() + "";
            db.Query(strSql);
            MessageBox.Show("Worker with ID " + listBox1.SelectedItem.ToString() + " deleted");
            RefreshWorkersTable();
        }
    }
}
