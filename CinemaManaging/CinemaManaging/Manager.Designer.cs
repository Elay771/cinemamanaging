﻿namespace CinemaManaging
{
    partial class Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Statistics = new System.Windows.Forms.Button();
            this.OrderTicket = new System.Windows.Forms.Button();
            this.CustomersData = new System.Windows.Forms.Button();
            this.WorkersData = new System.Windows.Forms.Button();
            this.HiManager = new System.Windows.Forms.Label();
            this.MoviesData = new System.Windows.Forms.Button();
            this.TheatersData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Statistics
            // 
            this.Statistics.Location = new System.Drawing.Point(332, 221);
            this.Statistics.Name = "Statistics";
            this.Statistics.Size = new System.Drawing.Size(144, 23);
            this.Statistics.TabIndex = 0;
            this.Statistics.Text = "View Statistics";
            this.Statistics.UseVisualStyleBackColor = true;
            this.Statistics.Click += new System.EventHandler(this.Statistics_Click);
            // 
            // OrderTicket
            // 
            this.OrderTicket.Location = new System.Drawing.Point(332, 250);
            this.OrderTicket.Name = "OrderTicket";
            this.OrderTicket.Size = new System.Drawing.Size(144, 23);
            this.OrderTicket.TabIndex = 1;
            this.OrderTicket.Text = "Order a Movie Ticket";
            this.OrderTicket.UseVisualStyleBackColor = true;
            this.OrderTicket.Click += new System.EventHandler(this.OrderMovieTicket);
            // 
            // CustomersData
            // 
            this.CustomersData.Location = new System.Drawing.Point(332, 279);
            this.CustomersData.Name = "CustomersData";
            this.CustomersData.Size = new System.Drawing.Size(144, 23);
            this.CustomersData.TabIndex = 2;
            this.CustomersData.Text = "Customers Data";
            this.CustomersData.UseVisualStyleBackColor = true;
            this.CustomersData.Click += new System.EventHandler(this.CustomersData_Click);
            // 
            // WorkersData
            // 
            this.WorkersData.Location = new System.Drawing.Point(332, 308);
            this.WorkersData.Name = "WorkersData";
            this.WorkersData.Size = new System.Drawing.Size(144, 23);
            this.WorkersData.TabIndex = 3;
            this.WorkersData.Text = "Workers Data";
            this.WorkersData.UseVisualStyleBackColor = true;
            this.WorkersData.Click += new System.EventHandler(this.WorkersData_Click);
            // 
            // HiManager
            // 
            this.HiManager.AutoSize = true;
            this.HiManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.HiManager.Location = new System.Drawing.Point(70, 29);
            this.HiManager.Name = "HiManager";
            this.HiManager.Size = new System.Drawing.Size(161, 31);
            this.HiManager.TabIndex = 4;
            this.HiManager.Text = "Hi Manager!";
            // 
            // MoviesData
            // 
            this.MoviesData.Location = new System.Drawing.Point(332, 338);
            this.MoviesData.Name = "MoviesData";
            this.MoviesData.Size = new System.Drawing.Size(144, 23);
            this.MoviesData.TabIndex = 5;
            this.MoviesData.Text = "Movies Data";
            this.MoviesData.UseVisualStyleBackColor = true;
            this.MoviesData.Click += new System.EventHandler(this.MoviesData_Click);
            // 
            // TheatersData
            // 
            this.TheatersData.Location = new System.Drawing.Point(332, 368);
            this.TheatersData.Name = "TheatersData";
            this.TheatersData.Size = new System.Drawing.Size(144, 23);
            this.TheatersData.TabIndex = 6;
            this.TheatersData.Text = "Theaters Data";
            this.TheatersData.UseVisualStyleBackColor = true;
            this.TheatersData.Click += new System.EventHandler(this.TheatersData_Click);
            // 
            // Manager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TheatersData);
            this.Controls.Add(this.MoviesData);
            this.Controls.Add(this.HiManager);
            this.Controls.Add(this.WorkersData);
            this.Controls.Add(this.CustomersData);
            this.Controls.Add(this.OrderTicket);
            this.Controls.Add(this.Statistics);
            this.Name = "Manager";
            this.Text = "Manager";
            this.Load += new System.EventHandler(this.Manager_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Statistics;
        private System.Windows.Forms.Button OrderTicket;
        private System.Windows.Forms.Button CustomersData;
        private System.Windows.Forms.Button WorkersData;
        private System.Windows.Forms.Label HiManager;
        private System.Windows.Forms.Button MoviesData;
        private System.Windows.Forms.Button TheatersData;
    }
}