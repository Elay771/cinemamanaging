﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MovieTicketOrder : Form
    {
        DB db;
        string WorkerID;
        public MovieTicketOrder(DB db, string WorkerID)
        {
            this.WorkerID=WorkerID;
            this.db = db;
            InitializeComponent();
        }
        private void MovieTicketOrder_Load(object sender, EventArgs e)
        {
            RefreshMovieTable();
        }

        private void RefreshMovieTable()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();

            db.Query("Select * from Movies;");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                listBox1.Items.Add(DB.ds.Tables[0].Rows[i]["MovieName"].ToString());
                listBox2.Items.Add(DB.ds.Tables[0].Rows[i]["Genre"].ToString());
                listBox3.Items.Add(DB.ds.Tables[0].Rows[i]["AgeLimit"].ToString());
                listBox4.Items.Add(DB.ds.Tables[0].Rows[i]["RunningTimeInMinutes"].ToString());
            }
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                listBox1.SelectedItem = listBox1.Items[0];
            this.Hide();
            var next = new ChooseMovieDate(db, DB.ds.Tables[0].Rows[listBox1.SelectedIndex]["MovieID"].ToString(),WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }
    }
}
