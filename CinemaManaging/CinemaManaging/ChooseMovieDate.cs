﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class ChooseMovieDate : Form
    {
        DB db;
        string movieID;
        string WorkerID;
        public ChooseMovieDate(DB db,string movieID, string WorkerID)
        {
            this.WorkerID = WorkerID;
            this.movieID = movieID;
            this.db = db;
            InitializeComponent();
        }

        private void ChooseMovieDate_Load(object sender, EventArgs e)
        {
            RefreshMovieOrdersTable();
        }
        private void RefreshMovieOrdersTable()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();

            db.Query("Select * from TheaterMovies where MovieID="+movieID+";");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                listBox4.Hide();
                listBox4.Items.Add(DB.ds.Tables[0].Rows[i]["TheaterMovieID"].ToString());
                listBox2.Items.Add(DB.ds.Tables[0].Rows[i]["StartTime"].ToString());
                listBox3.Items.Add(DB.ds.Tables[0].Rows[i]["TheaterID"].ToString());
                db.Query("Select * from Movies where MovieID=" + DB.ds.Tables[0].Rows[i]["MovieID"].ToString() + ";");
                listBox1.Items.Add(DB.ds.Tables[0].Rows[0]["MovieName"].ToString());
                db.Query("Select * from TheaterMovies;");
            }
        }
        private void Button1_Click_1(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                listBox1.SelectedItem = listBox1.Items[0];
            this.Hide();
            var next = new ChooseSeats(db, listBox4.Items[listBox1.SelectedIndex].ToString(),WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }
    }
}
