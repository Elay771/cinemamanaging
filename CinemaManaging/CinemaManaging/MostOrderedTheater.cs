﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MostOrderedTheater : Form
    {
        DB db;
        DateTime from, to;
        public MostOrderedTheater(DB db, DateTime from, DateTime to)
        {
            this.from = from;
            this.to = to;
            this.db = db;
            InitializeComponent();
        }

        private void MostOrderedTheater_Load(object sender, EventArgs e)
        {
            int max = -1;
            int maxTheaterID = -1;
            Dictionary<int, int> TheaterCounts = new Dictionary<int, int>();
            db.Query("Select * from MovieOrders");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                int id = Int32.Parse(DB.ds.Tables[0].Rows[i]["TheaterMovieID"].ToString());
                db.Query("Select * from TheaterMovies where TheaterMovieID=" + id.ToString() + ";");
                string date = DB.ds.Tables[0].Rows[0]["StartTime"].ToString().Substring(0, 10);
                DateTime currDate = DateTime.Parse(date);
                if (DateTime.Compare(from, currDate) <= 0 && DateTime.Compare(to, currDate) >= 0)
                {
                    db.Query("Select * from TheaterMovies where TheaterMovieID=" + id.ToString() + ";");
                    int theaterID = Int32.Parse(DB.ds.Tables[0].Rows[0]["TheaterID"].ToString());
                    if (TheaterCounts.ContainsKey(theaterID))
                        TheaterCounts[theaterID] += 1;
                    else
                        TheaterCounts.Add(theaterID, 1);
                }
                db.Query("Select * from MovieOrders");
            }
            foreach (KeyValuePair<int, int> entry in TheaterCounts)
            {
                if (max < entry.Value)
                {
                    max = entry.Value;
                    maxTheaterID = entry.Key;
                }
            }
            if (maxTheaterID >= 0)
            {
                db.Query("Select * from Theaters where TheaterID=" + maxTheaterID + ";");
                label5.Hide();
                label7.Text = DB.ds.Tables[0].Rows[0]["TheaterID"].ToString();
                label8.Text = DB.ds.Tables[0].Rows[0]["NumRows"].ToString();
                label9.Text = DB.ds.Tables[0].Rows[0]["NumCols"].ToString();
            }
            else
            {
                label2.Hide();
                label3.Hide();
                label4.Hide();
                
                label7.Hide();
                label8.Hide();
                label9.Hide();
               
                label5.Show();
            }
        }
    }
}
