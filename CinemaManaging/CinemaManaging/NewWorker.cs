﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace CinemaManaging
{
    public partial class NewWorker : Form
    {
        DB db;

        public NewWorker(DB db)
        {
            this.db = db;
            InitializeComponent();
            JoinError.Hide();
        }

        public void signUpByAccess(string access)
        {
            if (access.CompareTo("Manager") == 0)
            {
                this.Hide();
                var next = new Manager(db,EnteredID.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }

            if (access.Contains("Shift Manager"))
            {
                this.Hide();
                var next = new ShiftManager(db, EnteredID.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }

            if (access.CompareTo("Assistant Manager") == 0)
            {
                this.Hide();
                var next = new AssistantManager(db, EnteredID.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }

            if (access.CompareTo("Tickets Seller") == 0)
            {
                this.Hide();
                var next = new TicketSeller(db, EnteredID.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }

            if (access.CompareTo("Food Court Seller") == 0)
            {
                this.Hide();
                var next = new FoodCourtSeller(db, EnteredID.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }

            if (access.Contains("Ticket Checker"))
            {
                this.Hide();
                var next = new TicketChecker(db, EnteredID.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }
        }

        private void joinButtonClick(object sender, EventArgs e)
        {
            JoinError.Hide();
            
            if (Int32.TryParse(EnteredID.Text,out int i)&&i>0&&
                Int32.TryParse(EnteredAge.Text, out int j)&& i > 0&&
                Regex.IsMatch(EnteredName.Text, @"^[a-zA-Z]+$")&&
                EnteredPassword.Text.Length>=6
                &&(EnteredPosition.Text=="Manager"|| EnteredPosition.Text == "Tickets Seller"
                || EnteredPosition.Text == "Shift Manager"
                || EnteredPosition.Text == "Assistant Manager" || EnteredPosition.Text == "Ticket Checker" ||
                EnteredPosition.Text == "Food Court Seller")&& EnteredEmail.Text.Contains('@') && EnteredEmail.Text.Contains('.')
                && !db.doesUserExist(EnteredID.Text))
            {
                db.addUserToDB(EnteredName.Text, EnteredID.Text, EnteredPassword.Text, EnteredEmail.Text, EnteredAge.Text, EnteredPosition.Text);
                string access = EnteredPosition.Text;
                signUpByAccess(access);
            }
            else
            {
                JoinError.Show();
            }
        }

        private void NewWorker_Load(object sender, EventArgs e)
        {

        }
    }
}
