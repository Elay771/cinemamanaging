﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class TheatersDataShow : Form
    {
        DB db;
        public TheatersDataShow(DB db)
        {
            this.db = db;
            InitializeComponent();
        }

        private void TheatersDataShow_Load(object sender, EventArgs e)
        {
            RefreshWorkersTable();
        }
        private void RefreshWorkersTable()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();

            db.Query("Select * from Theaters");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                listBox1.Items.Add(DB.ds.Tables[0].Rows[i]["TheaterID"].ToString());
                listBox2.Items.Add(DB.ds.Tables[0].Rows[i]["NumRows"].ToString());
                listBox3.Items.Add(DB.ds.Tables[0].Rows[i]["NumCols"].ToString());
            }
        }

        private void Refresh_Click_1(object sender, EventArgs e)
        {
            RefreshWorkersTable();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                listBox1.SelectedItem = listBox1.Items[0];
            if (textBox1.Text != "" && textBox2.Text != "" &&
                textBox3.Text != "")
            {
                bool isSup = false;
                db.Query("Select * from Theaters");
                for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
                {
                    if (listBox1.SelectedItem.ToString() == DB.ds.Tables[0].Rows[i]["TheaterID"].ToString())
                    {
                        isSup = true;
                        break;
                    }

                }
                if (isSup)
                {
                    db.Query("Select * from Theaters");
                    List<string> statements = new List<string>();
                    statements.Add("UPDATE Theaters new SET [NumRows]=" + textBox2.Text + " WHERE TheaterID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    statements.Add("UPDATE Theaters new SET [NumCols]=" + textBox3.Text + " WHERE TheaterID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    for (int i = 0; i < 2; i++)
                        db.Query(statements.ToArray()[i]);
                    MessageBox.Show("Theater updated succesfully");
                    RefreshWorkersTable();
                }
                else
                    MessageBox.Show("Theater with ID: " + textBox1.Text + " is not in the system");
            }
        }

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            textBox1.Text = listBox1.Items[listBox1.SelectedIndex].ToString();
            textBox2.Text = listBox2.Items[listBox1.SelectedIndex].ToString();
            textBox3.Text = listBox3.Items[listBox1.SelectedIndex].ToString();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                listBox1.SelectedItem = listBox1.Items[0];
            string strSql = "DELETE FROM Theaters WHERE TheaterID =" + listBox1.SelectedItem.ToString() + "";
            db.Query(strSql);
            MessageBox.Show("Theater with ID " + listBox1.SelectedItem.ToString() + " deleted");
            RefreshWorkersTable();
        }
    }
}
