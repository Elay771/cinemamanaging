﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class Manager : Form
    {
        DB db;
        string WorkerID;
        public Manager(DB db, string WorkerID)
        {
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void Manager_Load(object sender, EventArgs e)
        {
        }

        private void Statistics_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new Statistics(db);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void OrderMovieTicket(object sender, EventArgs e)
        {
            this.Hide();
            var next = new MovieTicketOrder(db,WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void CustomersData_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new CustomerDataShow(db);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void WorkersData_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new WorkersDataShow(db);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void MoviesData_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new MoviesDataShow(db);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void TheatersData_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new TheatersDataShow(db);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }
    }
}
