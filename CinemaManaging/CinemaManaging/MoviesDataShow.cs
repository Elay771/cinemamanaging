﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MoviesDataShow : Form
    {
        DB db;
        public MoviesDataShow(DB db)
        {
            this.db = db;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                listBox1.SelectedItem = listBox1.Items[0];
            if (textBox1.Text != "" && textBox2.Text != "" &&
                textBox3.Text != "" &&
                textBox4.Text != "" &&
                textBox5.Text != "")
            {
                bool isSup = false;
                db.Query("Select * from Movies");
                for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
                {

                    if (listBox1.SelectedItem.ToString() == DB.ds.Tables[0].Rows[i]["MovieID"].ToString())
                    {
                        isSup = true;
                        break;
                    }

                }
                if (isSup)
                {
                    List<string> statements = new List<string>();
                    statements.Add("UPDATE Movies new SET [MovieName]='" + textBox2.Text + "'" + " WHERE MovieID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    statements.Add("UPDATE Movies new SET [Genre]='" + textBox3.Text + "'" + " WHERE MovieID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    statements.Add("UPDATE Movies new SET [AgeLimit]=" + textBox4.Text + " WHERE MovieID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    statements.Add("UPDATE Movies new SET [RunningTimeInMinutes]='" + textBox5.Text + "'" + " WHERE MovieID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    statements.Add("UPDATE Movies new SET [MovieID]=" + textBox1.Text + " WHERE MovieID =" + listBox1.SelectedItem.ToString());//פעולת העידכון
                    for (int i = 0; i < 5; i++)
                        db.Query(statements.ToArray()[i]);
                    MessageBox.Show("Movie updated succesfully");
                    RefreshMoviesTable();
                }
                else
                    MessageBox.Show("Movie with ID: " + textBox1.Text + " is not in the system");
            }
        }
        

        private void RefreshMoviesTable()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();

            db.CustomersData();
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                //listBox1.Items.Add(DB.ds.Tables[0].Rows[i]["MovieID"].ToString());
                listBox2.Items.Add(DB.ds.Tables[0].Rows[i]["MovieName"].ToString());
                listBox3.Items.Add(DB.ds.Tables[0].Rows[i]["Genre"].ToString());
                listBox4.Items.Add(DB.ds.Tables[0].Rows[i]["AgeLimit"].ToString());
                listBox5.Items.Add(DB.ds.Tables[0].Rows[i]["RunningTimeInMinutes"].ToString());
            }
        }
        private void Refresh_Click(object sender, EventArgs e)
        {
            RefreshMoviesTable();
        }

        private void MoviesDataShow_Load(object sender, EventArgs e)
        {
            RefreshMoviesTable();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                listBox1.SelectedItem = listBox1.Items[0];
            string strSql = "DELETE FROM Movies WHERE MovieID=" + listBox1.SelectedItem.ToString() + "";
            db.Query(strSql);
            MessageBox.Show("Movie with ID " + listBox1.SelectedItem.ToString() + " deleted");
            RefreshMoviesTable();
        }

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            textBox1.Text = listBox1.Items[listBox1.SelectedIndex].ToString();
            textBox2.Text = listBox2.Items[listBox1.SelectedIndex].ToString();
            textBox3.Text = listBox3.Items[listBox1.SelectedIndex].ToString();
            textBox4.Text = listBox4.Items[listBox1.SelectedIndex].ToString();
            textBox5.Text = listBox5.Items[listBox1.SelectedIndex].ToString();
        }
    }
}
