﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class LeastOrderedMovie : Form
    {
        DB db;
        DateTime from, to;
        public LeastOrderedMovie(DB db, DateTime from, DateTime to)
        {
            this.from = from;
            this.to = to;
            this.db = db;
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void LeastOrderedMovie_Load(object sender, EventArgs e)
        {
            int min = -1;
            int minMovieID = -1;
            Dictionary<int, int> MoviesCounts = new Dictionary<int, int>();
            db.Query("Select * from MovieOrders");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                int id = Int32.Parse(DB.ds.Tables[0].Rows[i]["TheaterMovieID"].ToString());
                db.Query("Select * from TheaterMovies where TheaterMovieID=" + id.ToString() + ";");
                string date = DB.ds.Tables[0].Rows[0]["StartTime"].ToString().Substring(0, 10);
                DateTime currDate = DateTime.Parse(date);
                if (DateTime.Compare(from, currDate) <= 0 && DateTime.Compare(to, currDate) >= 0)
                {
                    db.Query("Select * from TheaterMovies where TheaterMovieID=" + id.ToString() + ";");
                    int movieID = Int32.Parse(DB.ds.Tables[0].Rows[0]["MovieID"].ToString());
                    if (MoviesCounts.ContainsKey(movieID))
                        MoviesCounts[movieID] += 1;
                    else
                        MoviesCounts.Add(movieID, 1);
                }
                db.Query("Select * from MovieOrders");
            }
            foreach (KeyValuePair<int, int> entry in MoviesCounts)
            {
                if (min> entry.Value||min==-1)
                {
                    min= entry.Value;
                    minMovieID = entry.Key;
                }
            }
            if (minMovieID >= 0)
            {
                db.Query("Select * from Movies where MovieID=" + minMovieID + ";");
                label2.Hide();
                label7.Text = DB.ds.Tables[0].Rows[0]["MovieID"].ToString();
                label8.Text = DB.ds.Tables[0].Rows[0]["MovieName"].ToString();
                label9.Text = DB.ds.Tables[0].Rows[0]["Genre"].ToString();
                label10.Text = DB.ds.Tables[0].Rows[0]["AgeLimit"].ToString();
                label11.Text = DB.ds.Tables[0].Rows[0]["RunningTimeInMinutes"].ToString();
            }
            else
            {
                label16.Hide();
                label15.Hide();
                label14.Hide();
                label13.Hide();
                label12.Hide();
                label7.Hide();
                label8.Hide();
                label9.Hide();
                label10.Hide();
                label11.Hide();
                label12.Hide();
                label2.Show();
            }
        }
    }
}
