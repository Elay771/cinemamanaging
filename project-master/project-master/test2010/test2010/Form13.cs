﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace test2010
{
    public partial class Form13 : Form
    {
        int salary;
        string[] arr;
        bool selected = false;
        public Form13()
        {
            InitializeComponent();
        }

        private void Form13_Load(object sender, EventArgs e)
        {
            this.Text = "new worker";
            comboBox1.Items.AddRange(new string[] { "Cashier", "Cleaner", "goods organizer", "Security man","Shift Manager" });
            comboBox1.Text = "CHOOSE THE FUNCTION";
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && selected)
            {
                DataBase.Query("Select * from functions ");

                for (int i = 0; i < DataBase.ds.Tables[0].Rows.Count; i++)
                {
                    if (DataBase.ds.Tables[0].Rows[i]["function"].ToString() == comboBox1.SelectedItem.ToString())
                    {
                        salary = int.Parse(DataBase.ds.Tables[0].Rows[i]["salary"].ToString());
                        break;
                    }
                }
                string strSql = "INSERT INTO workers ([idW],[nameW],[phoneNumber],[HoursAMonth],[function],[salary]) VALUES ('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','" + comboBox1.SelectedItem.ToString() + "'," + salary + ")";
                DataBase.Query(strSql);
                MessageBox.Show("new worker added successfully");
                Class1.formARRS();
                Class1.openForm(3, this);
            }
            else
            {
                MessageBox.Show("Enter All data fields");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selected = true;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Class1.firstUpper(textBox2);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.TextLength == 10)
                textBox4.Focus();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            Class1.openForm(3, this);
        }

        
    }
}
