﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace test2010
{
    public partial class Form30 : Form
    {   public static int SlipNum;
        public Form30()
        {
            InitializeComponent();
        }

        private void Form30_Load(object sender, EventArgs e)
        {
            this.Text = "All transactions";
            Class1.printList1(4, "transactions", "slipNum", "date", "workerID", "costumerID", null, null, null, listBox1, listBox2, listBox3, listBox4, null, null, null, this);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SlipNum = int.Parse(listBox1.SelectedItem.ToString());
            Class1.openForm(31, this);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class1.openForm(1, this);
        }

    }
}
