﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace test2010
{
    public partial class Form6 : Form
    {
        public static string dateSt;
        public static string dateSh;
        public Form6()
        {
            InitializeComponent();
        }

        private void Form6_Load(object sender, EventArgs e)
        {   
            button1.Hide();
            string datenow = DateTime.Now.ToString("d/M/yyyy").ToString();
            MessageBox.Show(datenow);

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            
            if(textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
            {
             DateTime dateO = new DateTime(int.Parse(textBox3.Text), int.Parse(textBox2.Text), int.Parse(textBox1.Text), 0, 0, 0);
             dateSt = dateO.ToString();//full date
             dateSh = dateO.ToShortDateString();//without hour
             Class1.openForm(7,this);
            
          
            }   else
            MessageBox.Show("fill all boxes");
         
                
                


         
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.TextLength == 2)
            {
                textBox2.Focus();
            }

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.TextLength == 2)
            {
                textBox3.Focus();
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.TextLength == 4)
            {
                button1.Show();
                button1.Focus();
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            string date2 = dateTimePicker1.Value.ToShortDateString();
            MessageBox.Show(date2);
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            string date2 = monthCalendar1.SelectionRange.Start.ToShortDateString();
            MessageBox.Show(date2);
        }

        
    }
}
