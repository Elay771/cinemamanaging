﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace test2010
{
    public partial class Form17 : Form
    {
        public static int workerId;
        public static int costumerId;
        public bool isInData = false;
        public static string textBox2Copy;
        public  static int slipNum;
       
        public Form17()
        {
            InitializeComponent();
        }

        private void Form17_Load(object sender, EventArgs e)
        {
            this.Text = "New purchase";
            if (Form27.form27)
            {
                textBox1.Text = workerId.ToString();
                textBox2.Text = textBox2Copy;
           

            }
            
                
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox2Copy = textBox2.Text;
            DataBase.Query("Select * from transactions ");
             slipNum = DataBase.ds.Tables[0].Rows.Count + 1;
             
            if (textBox1.Text != "" )
            {
                workerId = int.Parse(textBox1.Text);
                
                DataBase.Query("Select * from workers ");
                for (int i = 0; i < DataBase.ds.Tables[0].Rows.Count; i++)
                {

                    if (textBox1.Text == DataBase.ds.Tables[0].Rows[i]["idW"].ToString())
                    {
                        isInData = true;
                        break;
                    }

                }
                if (!isInData)
                {
                    MessageBox.Show("no worker with " + workerId + " id" + "\n" + "please enter new id");

                    return;
                }
                DataBase.Query("Select * from costumers ");
                isInData = false;
                for (int i = 0; i < DataBase.ds.Tables[0].Rows.Count; i++)
                {

                    if (textBox2.Text == DataBase.ds.Tables[0].Rows[i]["costumerID"].ToString())
                    {
                        isInData = true;
                        break;
                    }

                }
                if (!isInData)
                {
                    DialogResult dialogResult = MessageBox.Show("do you want to add customer id to system(customers in system get a 10 precent discount), ", "customer no in system", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        Class1.openForm(27, this);
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        textBox2.Text= "";
                    }
                
                
                }
                if (textBox2.Text.Length > 0)
                    costumerId = int.Parse(textBox2.Text);
                else
                    costumerId = 0;
               
                Class1.openForm(16, this);
            
            }
            else
            {
                MessageBox.Show("Enter All data fields");
            }

        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Class1.openForm(28, this);
        }
      
    }
}
