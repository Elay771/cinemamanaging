﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace test2010
{
    public partial class Form29 : Form
    {
        public static bool crPas;
        int password;
        public Form29()
        {
            InitializeComponent();
        }

        private void Form29_Load(object sender, EventArgs e)
        {
            this.Text = "Login";
            password = 1;
            button2.Text = char.ConvertFromUtf32(0x2190);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                if (int.Parse(textBox1.Text) == password)
                {
                    crPas = true;
                    Class1.openForm(1, this);

                }
                else
                    MessageBox.Show("wrong password");
                textBox1.Focus();
            }
            else
                MessageBox.Show("enter password");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Class1.firstUpper(textBox1);

        }

        private void button1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (int.Parse(textBox1.Text) == password)
                {
                    crPas = true;
                    Class1.openForm(1, this);

                }
                else
                    MessageBox.Show("wrong password");

            }
        }



    }
}