﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace test2010
{
    public partial class Form27 : Form
    {   public static bool form27;
        public Form27()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
            {
                string strSql = "INSERT INTO costumers ([costumerID],[nameC],[phone],[age]) VALUES ('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "')";
                DataBase.Query(strSql);
                MessageBox.Show("new customers added successfully");
                form27 = true;
                Class1.formARRS();
                Class1.openForm(17, this);
            }
            else
            {
                MessageBox.Show("Enter All data fields");
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.TextLength == 10)
            {
                textBox4.Focus();
            }
        }

        private void Form27_Load(object sender, EventArgs e)
        {
            this.Text ="New customer";
            form27 = false;
            textBox1.Text = Form17.textBox2Copy;
            textBox2.Focus();
           
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Class1.firstUpper(textBox2);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Class1.openForm(17, this);
        }
       
    }
}
