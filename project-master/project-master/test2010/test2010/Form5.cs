﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace test2010
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {     
            this.Text = "sellers of id: " + Form2.id1;
            DataBase.Query("Select * from workers");
            int rowsW = DataBase.ds.Tables[0].Rows.Count;
            DataBase.Query("Select * from transactions");
            int u = DataBase.ds.Tables[0].Rows.Count;

            for (int i = 0; i < u; i++)
            {
                DataBase.Query("Select * from transactions");
                string SelIDC = DataBase.ds.Tables[0].Rows[i]["costumerID"].ToString();

                if (Form2.id1 == SelIDC)
                {
                    listBox5.Items.Add(DataBase.ds.Tables[0].Rows[i]["date"].ToString());
                    string SIdW = DataBase.ds.Tables[0].Rows[i]["workerID"].ToString();
                    for (int y = 0; y < rowsW; y++)
                    {
                        DataBase.Query("Select * from workers");
                        string c = DataBase.ds.Tables[0].Rows[y]["IdW"].ToString();
                        if (SIdW == c)
                        {
                            listBox1.Items.Add(SIdW);
                            listBox2.Items.Add(DataBase.ds.Tables[0].Rows[y]["nameW"].ToString());
                            listBox3.Items.Add(DataBase.ds.Tables[0].Rows[y]["phoneNumber"].ToString());
                            listBox4.Items.Add(DataBase.ds.Tables[0].Rows[y]["function"].ToString());
                        }
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class1.openForm(1, this);
        }
    }
}
