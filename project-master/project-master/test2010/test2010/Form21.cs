﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace test2010
{
    public partial class Form21 : Form
    {   bool isSup=false;
        public static string nameP;
        public static int itemsToHandle;

        public Form21()
        {
            InitializeComponent();
        }

        private void Form21_Load(object sender, EventArgs e)
        {
            this.Text = "Products";
            textBox1.Hide();
            label3.Hide();
            comboBox2.Hide();
            button1.Hide();

            if (Form10.form10)
            {
                button2.Hide();
                label3.Show();
                comboBox2.Show();
                comboBox2.Items.AddRange(new string[] { "UPDATE ", "DELETE" });
                comboBox2.Text = "CHOOSE THE OPERATION";
                DataBase.Query("Select * from Stock");
                for (int i = 0; i < DataBase.ds.Tables[0].Rows.Count; i++)
                    if (Form8.idS == DataBase.ds.Tables[0].Rows[i]["SupID"].ToString())
                    {
                        listBox1.Items.Add(DataBase.ds.Tables[0].Rows[i]["nameP"].ToString());
                        listBox2.Items.Add(DataBase.ds.Tables[0].Rows[i]["amountInStack"].ToString());
                        listBox3.Items.Add(DataBase.ds.Tables[0].Rows[i]["MinimunAmount"].ToString());
                        listBox4.Items.Add(DataBase.ds.Tables[0].Rows[i]["PriceOfSelling"].ToString());
                        listBox5.Items.Add(DataBase.ds.Tables[0].Rows[i]["PriceOfBuying"].ToString());
                        listBox6.Items.Add(DataBase.ds.Tables[0].Rows[i]["SupID"].ToString());
                        label1.Text = "choose action, to delete or update first select product name" + "\n" + "Or if you want to make an action for all related products select from the group action options";
                      




                    }

            }

            else
                Class1.printList1(6, "Stock", "nameP", "amountInStack", "MinimunAmount", "PriceOfSelling", "PriceOfBuying", "SupID", null, listBox1, listBox2, listBox3, listBox4, listBox5, listBox6, null, this);

            comboBox1.Items.AddRange(new string[] { "ADD NEW", "DEL EXIST RECORD", "UPDATE RECORD" });
            comboBox1.Text = "CHOOSE THE OPERATION";
        }





        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    Class1.openForm(22, this);

                    break; // TO add
                case 1:
                    if (nameP != null)
                    {

                        Class1.openForm(23, this);

                    }
                    else
                        MessageBox.Show("please select product name");
                    break; // TO delete
                case 2:
                    if (nameP != null)
                    {

                        Class1.openForm(24, this);

                    }
                    else
                        MessageBox.Show("please select product name");

                    break; // TO update
            }// switch
        }

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            DataBase.Query("select * from Stock where nameP ='" + listBox1.SelectedItem + "'");// לחיצה על שם הספק אותו אנו רוצים למחוק או לעדכן
            nameP = DataBase.ds.Tables[0].Rows[0]["nameP"].ToString();// שמירת השם במשתנה

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            itemsToHandle = listBox1.Items.Count;
            switch (comboBox2.SelectedIndex)
            {
                case 0:
                    foreach (var lbl in Controls.OfType<Label>())
                        lbl.Hide();
                    foreach (var lst in Controls.OfType<ListBox>())
                        lst.Hide();
                    foreach (var cb in Controls.OfType<ComboBox>())
                        cb.Hide();
                    textBox1.Show();
                    label1.Show();
                    label1.Text="To what supplier id would you like to change product"+"\n\n"+"enter new supplier id and then press the button";
                    button1.Show();
                    break; // TO update
                case 1://delete all

                    for (int i = 0; i < itemsToHandle; i++)
                    {
                        nameP = listBox1.Items[i].ToString();
                        string strSql = "DELETE FROM Stock WHERE nameP ='" + Form21.nameP + "'";
                        DataBase.Query(strSql);
                        
                    }
                    MessageBox.Show("all items were deleted");
                    
                    Class1.openForm(8, this);
                    break;

            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                DataBase.Query("Select * from suppliers");
                for (int i = 0; i < DataBase.ds.Tables[0].Rows.Count; i++)
                {

                    if (textBox1.Text == DataBase.ds.Tables[0].Rows[i]["id"].ToString())
                    {
                        isSup = true;
                        break;
                    }
   
                }
                if (isSup)
                {
                    for (int i = 0; i < itemsToHandle; i++)
                    {
                        nameP = listBox1.Items[i].ToString();
                        string strSql = "UPDATE Stock new SET [SupID]='" + textBox1.Text + "' " + " WHERE nameP ='" + nameP + "'";//פעולת העידכון
                        DataBase.Query(strSql);

                    }
                    MessageBox.Show("products updated succesfully");
                    Class1.openForm(8, this);
                }
                else
                                        MessageBox.Show("supllier with id: " + textBox1.Text + " is not in the system");
            }
            else
                MessageBox.Show("Enter All data fields");        
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Class1.openForm(25, this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Class1.openForm(1, this);
        } 
    }   
}

