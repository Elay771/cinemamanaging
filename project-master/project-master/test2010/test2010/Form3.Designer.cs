﻿namespace test2010
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.function = new System.Windows.Forms.ListBox();
            this.hoursAMonth = new System.Windows.Forms.ListBox();
            this.phone = new System.Windows.Forms.ListBox();
            this.name = new System.Windows.Forms.ListBox();
            this.id = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.salary = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // function
            // 
            this.function.FormattingEnabled = true;
            this.function.ItemHeight = 20;
            this.function.Location = new System.Drawing.Point(675, 154);
            this.function.Name = "function";
            this.function.Size = new System.Drawing.Size(120, 164);
            this.function.TabIndex = 13;
            // 
            // hoursAMonth
            // 
            this.hoursAMonth.FormattingEnabled = true;
            this.hoursAMonth.ItemHeight = 20;
            this.hoursAMonth.Location = new System.Drawing.Point(520, 154);
            this.hoursAMonth.Name = "hoursAMonth";
            this.hoursAMonth.Size = new System.Drawing.Size(120, 164);
            this.hoursAMonth.TabIndex = 12;
            // 
            // phone
            // 
            this.phone.FormattingEnabled = true;
            this.phone.ItemHeight = 20;
            this.phone.Location = new System.Drawing.Point(380, 154);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(120, 164);
            this.phone.TabIndex = 11;
            // 
            // name
            // 
            this.name.FormattingEnabled = true;
            this.name.ItemHeight = 20;
            this.name.Location = new System.Drawing.Point(232, 154);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(120, 164);
            this.name.TabIndex = 10;
            // 
            // id
            // 
            this.id.FormattingEnabled = true;
            this.id.ItemHeight = 20;
            this.id.Location = new System.Drawing.Point(88, 154);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(120, 164);
            this.id.TabIndex = 9;
            this.id.SelectedIndexChanged += new System.EventHandler(this.id_SelectedIndexChanged_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 126);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 20);
            this.label1.TabIndex = 14;
            this.label1.Text = "id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 126);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 20);
            this.label2.TabIndex = 15;
            this.label2.Text = "name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(380, 126);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "phone";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(520, 125);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 20);
            this.label4.TabIndex = 17;
            this.label4.Text = "hours a month";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(675, 123);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 20);
            this.label5.TabIndex = 18;
            this.label5.Text = "function";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(292, 14);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(393, 40);
            this.label6.TabIndex = 19;
            this.label6.Text = "choose action, to delete or update first select worker id\r\n\r\n";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(297, 57);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(180, 28);
            this.comboBox1.TabIndex = 20;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(834, 123);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 20);
            this.label7.TabIndex = 22;
            this.label7.Text = "salary";
            // 
            // salary
            // 
            this.salary.FormattingEnabled = true;
            this.salary.ItemHeight = 20;
            this.salary.Location = new System.Drawing.Point(834, 154);
            this.salary.Name = "salary";
            this.salary.Size = new System.Drawing.Size(120, 164);
            this.salary.TabIndex = 21;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(808, 349);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 47);
            this.button1.TabIndex = 23;
            this.button1.Text = "Back to menu";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 408);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.salary);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.function);
            this.Controls.Add(this.hoursAMonth);
            this.Controls.Add(this.phone);
            this.Controls.Add(this.name);
            this.Controls.Add(this.id);
            this.Name = "Form3";
            this.Text = "Form3";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox function;
        private System.Windows.Forms.ListBox hoursAMonth;
        private System.Windows.Forms.ListBox phone;
        private System.Windows.Forms.ListBox name;
        private System.Windows.Forms.ListBox id;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox salary;
        private System.Windows.Forms.Button button1;
    }
}