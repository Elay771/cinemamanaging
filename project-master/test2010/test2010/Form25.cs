﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace test2010
{
    public partial class Form25 : Form
    {   public static int productsToOrder;
        public static Stack<int> productId;
        public Form25()
        {
            InitializeComponent();
        }

        private void Form25_Load(object sender, EventArgs e)
        {
            productId = new Stack<int>();
            DataBase.Query("Select * from Stock");
            for (int i = 0; i < DataBase.ds.Tables[0].Rows.Count; i++)
            {
                if (int.Parse(DataBase.ds.Tables[0].Rows[i]["MinimunAmount"].ToString()) > int.Parse(DataBase.ds.Tables[0].Rows[i]["amountInStack"].ToString()))
                {
                   
                    listBox1.Items.Add(DataBase.ds.Tables[0].Rows[i]["nameP"].ToString() + " amount in stock: " + DataBase.ds.Tables[0].Rows[i]["amountInStack"].ToString() + ", the minimum amount is: " + DataBase.ds.Tables[0].Rows[i]["MinimunAmount"].ToString());
                    productId.Push(i);
                }
            
            }
            productsToOrder = listBox1.Items.Count;
        
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class1.openForm(26, this);
        }
    }
}
