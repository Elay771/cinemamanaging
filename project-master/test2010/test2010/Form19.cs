﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace test2010
{
    public partial class Form19 : Form
    {   public static int Case;
        public static DateTime startDate;
        public static DateTime endDate;

        public Form19()
        {
            InitializeComponent();
        }


        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            Case = 1;
            
        }


       
       

        private void Form19_Load(object sender, EventArgs e)
        {
            groupBox2.Hide();
            startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            startDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0); ;
            
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            endDate =new DateTime( dateTimePicker2.Value.Year,dateTimePicker2.Value.Month,dateTimePicker2.Value.Day,0,0,0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
        

            if (DateTime.Compare(startDate, endDate) <= 0)
            { Class1.openForm(20, this); }
            else
                MessageBox.Show("notice the date on the left is the starting date, cant be bigger the end date");

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            Case= 2;
        }
       

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            groupBox2.Show();

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            Case = 3;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            Case = 4;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            Case = 5;
        }
        

    }
}
