﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;//we must to add
namespace test2010
{
    class DataBase
    {
        public static System.Data.DataSet ds;
        public static OleDbConnection objConn;

        public static bool Connect(string DBFile)
        {
            try
            {
                DataBase.objConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + DBFile);
                return (objConn.State == System.Data.ConnectionState.Open);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
        }

        public static bool Query(string sqlStr)
        {
            try
            {
                DataBase.ds = new System.Data.DataSet();
                OleDbDataAdapter da = new OleDbDataAdapter(sqlStr, DataBase.objConn);
                da.Fill(ds);
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
