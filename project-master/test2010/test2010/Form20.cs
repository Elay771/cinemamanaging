﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace test2010
{
    public partial class Form20 : Form
    {
        bool stockCheck=true;
        int income;
        int outcome;
        int slipNum;
        int amountOfProducts;
        bool isData=false;
        
        public static strint[] products;

        public Form20()
        {
            InitializeComponent();
        }

        private void Form20_Load(object sender, EventArgs e)
        {

            DataBase.Query("Select * from transactions");
            int rows = DataBase.ds.Tables[0].Rows.Count;
            products = new strint[100];
            Class1.arrProductReset(products);
            for (int i = 0; i < rows; i++)
            {
                DataBase.Query("Select * from transactions");
                DateTime d1 = DateTime.Parse(DataBase.ds.Tables[0].Rows[i]["date"].ToString());



                if (DateTime.Compare(Form19.startDate, d1) <= 0 && DateTime.Compare(Form19.endDate, d1) >= 0)
                {
                    isData = true;
                    slipNum = int.Parse(DataBase.ds.Tables[0].Rows[i]["slipNum"].ToString());
                    switch (Form19.Case)
                    {
                        case 1:
                            
                            DataBase.Query("Select * from purchase");
                            int rowsPur = DataBase.ds.Tables[0].Rows.Count;
                            for (int y = 0; y < rowsPur; y++)
                            {
                                DataBase.Query("Select * from purchase");
                                if (int.Parse(DataBase.ds.Tables[0].Rows[y]["slipNum"].ToString()) == slipNum)
                                {

                                    string product = DataBase.ds.Tables[0].Rows[y]["product"].ToString();
                                    int amount = int.Parse(DataBase.ds.Tables[0].Rows[y]["amount"].ToString());

                                    DataBase.Query("select * from Stock WHERE nameP ='" + product + "'");
                                    income += int.Parse(DataBase.ds.Tables[0].Rows[0]["PriceOfSelling"].ToString()) * amount;
                                    outcome += int.Parse(DataBase.ds.Tables[0].Rows[0]["PriceOfBuying"].ToString()) * amount;
                                }

                            }
                            label1.Text = "the income between " + Form19.startDate.ToShortDateString() + " and " + Form19.endDate.ToShortDateString() + " is " + income.ToString() + " ILS" + "/n/n"
                                          + "the outcome between " + Form19.startDate.ToShortDateString() + " and " + Form19.endDate.ToShortDateString() + " is " + outcome.ToString() + " ILS/n/n"
                                          + "the profit is: " + (income - outcome) + "ILS";
                            break;
                        case 2:
                          
                            if (stockCheck == true)
                            {
                                stockCheck = false;
                                DataBase.Query("Select * from Stock");
                                for (int y = 0; y < DataBase.ds.Tables[0].Rows.Count; y++)
                                {
                                    amountOfProducts++;
                                    products[y] = new strint(DataBase.ds.Tables[0].Rows[y]["nameP"].ToString(), 0);

                                }
                            }
                            DataBase.Query("Select * from purchase");
                            for (int y = 0; y < DataBase.ds.Tables[0].Rows.Count; y++)
                            {
                                DataBase.Query("Select * from purchase");
                                if (int.Parse(DataBase.ds.Tables[0].Rows[y]["slipNum"].ToString()) == slipNum)
                                    for (int f = 0; f < amountOfProducts; f++)
                                        if (products[f].StringValue != null)
                                            if (DataBase.ds.Tables[0].Rows[y]["product"].ToString().Equals(products[f].StringValue.ToString()))
                                                products[f].IntValue += int.Parse(DataBase.ds.Tables[0].Rows[y]["amount"].ToString());
                            }

                            break;
                        case 3:
                            
                            if (stockCheck == true)
                            {
                                DataBase.Query("Select * from workers");
                                for (int y = 0; y < DataBase.ds.Tables[0].Rows.Count; y++)
                                {
                                    stockCheck = false;
                                    amountOfProducts++;
                                    products[y] = new strint(DataBase.ds.Tables[0].Rows[y]["idW"].ToString(), 0);

                                }
                            }
                            for (int y = 0; y < amountOfProducts; y++)
                            {

                                DataBase.Query("select * from transactions");
                                if (DataBase.ds.Tables[0].Rows[i]["workerID"].ToString() == products[y].StringValue)
                                    products[y].IntValue++;

                            }



                            break;
                        case 4:
                            
                            if (stockCheck == true)
                            {
                                DataBase.Query("Select * from costumers");
                                for (int y = 0; y < DataBase.ds.Tables[0].Rows.Count; y++)
                                {
                                    stockCheck = false;
                                    amountOfProducts++;
                                    products[y] = new strint(DataBase.ds.Tables[0].Rows[y]["costumerID"].ToString(), 0);

                                }
                            }
                            for (int y = 0; y < amountOfProducts; y++)
                            {

                                DataBase.Query("select * from transactions");
                                if (DataBase.ds.Tables[0].Rows[i]["costumerID"].ToString() == products[y].StringValue)
                                    products[y].IntValue++;

                            }
                            break;

                    }
                }



            }
            if (Form19.Case == 2)
            {
                for (int y = 0; y < amountOfProducts; y++)
                    if (products[y].StringValue != null)
                        listBox1.Items.Add(products[y].StringValue.ToString() + ":  " + products[y].IntValue.ToString());

                Stack<strint> st = findMax(products, amountOfProducts);
                while (st.Count != 0)
                    listBox1.Items.Add("the most sold product was : " + st.Peek().StringValue + " it was sold " + st.Pop().IntValue + " times");
                Stack<strint> stMin = findMin(products, amountOfProducts);
                while (stMin.Count != 0)
                    listBox2.Items.Add("the less sold product was: " + stMin.Peek().StringValue + " it was sold " + stMin.Pop().IntValue + " times");

            }
            if (Form19.Case == 3)
            {
                Stack<strint> st = findMax(products, amountOfProducts);
                if (st.Count > 1)
                    label1.Text = "the workers who helped most people between selcted dates are";
                else
                    label1.Text = "the worker who helped most people between selcted dates are";

                while (st.Count != 0)
                    listBox1.Items.Add("id number: " + st.Peek().StringValue + " helped " + st.Pop().IntValue + " customers");
                Stack<strint> stMin = findMin(products, amountOfProducts);
                while (stMin.Count != 0)
                    listBox2.Items.Add("id number: " + stMin.Peek().StringValue + " helped " + stMin.Pop().IntValue + " customers");
            }
            if (Form19.Case == 4)
            {
                Stack<strint> st = findMax(products, amountOfProducts);
                if (st.Count > 1)
                    label1.Text = "the customers who bought the most times between selcted dates are";
                else if (st.Count == 1) 
                    label1.Text = "the customer who bought the most times between selcted dates are";

                while (st.Count != 0)
                    listBox1.Items.Add("id number: " + st.Peek().StringValue + " bought  " + st.Pop().IntValue + " times");
                Stack<strint> stMin = findMin(products, amountOfProducts);
                while (stMin.Count != 0)
                    listBox2.Items.Add("id number: " + stMin.Peek().StringValue + " bought " + stMin.Pop().IntValue + " times");

            }
            if (!isData)
            {
                DialogResult dialogResult = MessageBox.Show("do you want to select other dates or action?", "no available data", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Class1.openForm(19, this);
                }
                else if (dialogResult == DialogResult.No)
                {
                    Application.Exit();
                }


            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }
        public static Stack<strint> findMax(strint[] p1, int lenght)
        {
            
            Stack<strint>Smax = new  Stack<strint>();
            Smax.Push(p1[0]);

            for (int i = 1; i < lenght; i++)
            {
                if (p1[i].IntValue > Smax.Peek().IntValue)
                {
                    
                    Smax.Clear();
                    Smax.Push(p1[i]);

                }
                else if (p1[i].IntValue == Smax.Peek().IntValue)
                    Smax.Push(p1[i]);
            }

            return Smax;


        }
        public static Stack<strint> findMin(strint[] p1, int lenght)
        {
            Stack<strint> Smin = new Stack<strint>();
            Smin.Push(p1[0]);

            for (int i = 1; i < lenght; i++)
            {
                if (p1[i].IntValue < Smin.Peek().IntValue)
                {

                    Smin.Clear();
                    Smin.Push(p1[i]);

                }
                else if (p1[i].IntValue == Smin.Peek().IntValue)
                    Smin.Push(p1[i]);
            }

            return Smin;

        }
        
    }
}
